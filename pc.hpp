#ifndef MECATRONIQUE_MAIN_PC_HPP
#define MECATRONIQUE_MAIN_PC_HPP

#include <vector>

namespace cv {
	class Vec2d;
	class Vec4d;
	class Mat;
};


/** \brief Représente les paramètres de intrsèques d'une camera calibrée
 * \sa mtCalibrateCameraChAruco
 */
struct mtCameraParams_t {
	uint32_t width;
	uint32_t height;
	float aspectRatio;
	uint32_t flags;
	float cameraMatrix[9];
	float distortionCoeffs[5];
};


/** \brief Calibrer une caméra
 * 
 * Cette fonction permet de calibrer une caméra à partir d'une série de clichés d'une matrice ChAruco.
 * 
 * \return paramètre d'une caméra calibrée
 */
mtCameraParams_t mtCalibrateCameraChAruco();


/** \brief Récuprer la position d'un robot
 * 
 * Cette fonction peremt de récupérer la position et l'orientation d'un robot dans le repère carte.
 * 
 * \param team indique le robot dont la pose doit être obtenu
 * \param position [sortie] position du robot dans le repère carte
 * \param rotation [sortie] rotation du robot dans le repère carte
 */
void mtGetRobotPose(int team, cv::Vec2d *position, float *rotation);


/** \brief 
 * 
 * Cette fonction permet de détecter les plantes dans la carte. La fonction retourne un tableau de
 * vecteurs-4 reprénsentant une boîte englobant les plantes (minx, miny, maxx, maxy).
 * 
 * \param image matrice de pixels représantant une image acquise depuis une caméra
 * \return tableau de vecteurs contenant les boîtes englobant les plantes. 
 */
std::vector<cv::Vec4d> mtDetectPlantsRegions(cv::Mat &image);


/** \brief Calculer la matrice du repère carte
 * 
 * Cette fonction permet d'obtenir la matrice de transformation de l'espace caméra à
 * l'espace carte.
 * 
 * \param t position de l'origine de la carte dans le repère caméra
 * \param r rotation de l'origine de la carte dans le repère caméra
 * \return matrice de transformation du repère caméra vers le repère carte
 */
cv::Mat mtCalculateTransformationMatrixGameBoard(cv::Vec3d &t, cv::Vec3d &r);

#endif //MECATRONIQUE_MAIN_PC_HPP
