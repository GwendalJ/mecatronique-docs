#ifndef MECATRONIQUE_PANEL_HPP
#define MECATRONIQUE_PANEL_HPP

#include "common.hpp"

class WiFiClient;

/** \brief Initialiser la caméra
 * 
 * Cette fonction permet d'initialiser le module caméra OV2640.
 * 
 * \sa mtCaptureAndSendFrame
 */
bool mtInitCamera();


/** \brief Envoyer une image
 * 
 * Cette fonction permet de prendre une photo et de l'envoyer au serveur TCP.
 * La caméra et la connexion avec le serveur doivent être initialisées.
 * 
 * /return true en cas de succès, false en cas d'échec
 * 
 * \sa mtInitCamera, mtInitWiFiAndConnect
 */
bool mtCaptureAndSendFrame();


/** \brief Connecter un serveur TCP via WiFi
 * 
 * Cette fonction permet de se connecter à un réseau WiFi, puis de se connecter à un serveur TCP.
 * 
 * \param ssid Nom du réseau WiFi auquel se connecter
 * \param password Mot de passe du réseau WiFi auquel se connecter
 * \param address IP du serveur (eg: `{192, 168, 137, 3}`)
 * \param port Port d'écoute du serveur (compris entre 0 et 65535)
 * \param timeout délais d'attente maximal en millisecondes, 0 = délais infini
 * 
 * /return true en cas de succès, false en cas d'échec
 */
bool mtInitWiFiAndConnect(char *ssid, char *password, uint8_t *address, uint16_t port, uint32_t timeout = 0);

/** \brief Récupérer la connexion au serveur
 * 
 * Cette fonction permet d'accéder au socket pour intérgir avec le serveur.
 * 
 * \return le socket de la connexion au serveur
 */
WiFiClient *getSocket();

/** \brief Recevoir un message du serveur
 * 
 * Cette fonction permet de recevoir un message depuis le serveur, la fonction bloque jusqu'à
 * la réception d'un message ou l'écoulement de la durée d'attente.
 * La connexion avec le serveur doit être initialisée.
 * 
 * \param len longueur du message en byte
 * \param timeout délais d'attente maximal en millisecondes, 0 = délais infini
 * 
 * \return message du serveur ou `nullptr` si aucun message n'a été reçu
 */
char *receiveMessageTCP(uint16_t *len, uint32_t timeout = 0);


/** \brief Initialiser le servo-moteur & moteur DC
 * 
 * Cette fonction permet d'initialiser le moteur DC et le servo-moteur servant à orienter les panneaux solaires.
 * 
 * \sa mtSetPanelWheelPosition, mtStartPanelWheel, mtStopPanelWheel
 */
void mtInitPanelMotors();


/** \brief Modifier la hauteur de la roue des panneaux
 * 
 * Cette fonction permet de monter ou d'abaisser la roue d'orientation des panneaux.
 * 
 * \param position UP permet dégager la roue du panneau (position haute), DOWN permet d'appuyer la roue contre le panneau (position basse). Les autres positions sont ignorées
 */
void mtSetPanelWheelPosition(mtPosition_t position);


/** \brief Récuprérer l'orientation actuelle du panneau
 * 
 * Cette fonction permet d'obtenir l'angle actuel du panneau à partir de son tag Aruco.
 * L'angle est exprimé en degré où 0° correspond à au panneau dans le sens du tag Aruco, et tourne dans le sens trigonométrique.
 * 
 * \return orientation actuelle du panneau en degré. L'angle est compris entre 0° et 360°. Une valeur de -1 représente un échec dans la lecture de l'orientation
 */
float mtGetPanelOrientation();


/** \brief Démarrer la roue du panneau
 * 
 * Cette fonction permet de démarrer la roue permettant d'orienter un panneau.
 */
void mtStartPanelWheel();


/** \brief Arrêter la roue du panneau
 * 
 * Cette fonction permet de stopper la roue permettant d'orienter un panneau.
 */
void mtStopPanelWheel();

#endif //MECATRONIQUE_PANEL_HPP
