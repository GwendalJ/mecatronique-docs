#ifndef MECATRONIQUE_COMMON_HPP
#define MECATRONIQUE_COMMON_HPP

#include <cstdint>


// --- DÉBUT DE LA DÉFINITION DES PINS --- //
#define PIN_ROBOT_LIDAR_FRONT_LEFT        12
#define PIN_ROBOT_LIDAR_FRONT_RIGHT       11
#define PIN_ROBOT_LIDAR_REAR        			10
#define PIN_ROBOT_CAROUSEL_TOP0        		9
#define PIN_ROBOT_SERVO_GRIPPER        		6
#define PIN_ROBOT_STEPPER_DIR 	       		4
#define PIN_ROBOT_STEPPER_PULL        		3

#define PIN_PANEL_SERVO         1
#define PIN_PANEL_DC_MOTOR      3
// ---- FIN DE LA DÉFINITION DES PINS ---- //


/** \brief Représente une position
 */
enum mtPosition_t {
	UP,
	DOWN
};


/** \brief Représente une équipe
 */
enum mtTeam_t {
	BLUE,
	YELLOW
};

#endif //MECATRONIQUE_COMMON_HPP
