#ifndef MECATRONIQUE_MAIN_ESP_HPP
#define MECATRONIQUE_MAIN_ESP_HPP

#include "common.hpp"

namespace cv {
	class Vec3d;
	class Mat;
};

typedef struct OperatingMode mtDynamixelMode_t;

/** \brief Représente un capteur de distance
 */
struct mtObstacleSensor_t {
	int pin;									/**< numéro du pin sur lequel est branché le capteur */
	int minDistance;					/**< distance en mm pour laquelle le capteur renvoie 0 */
	int maxDistance;					/**< distance en mm pour laquelle le capteur renvoie 255 */
};


/** \brief Représente un obstacle détecté
 * 
 * \sa mtDetectObstacleFront mtDetectObstacleRear
 */
struct mtObstableResult_t {
	float distance;						/**< distance entre l'origine du robot et l'obstacle */
	float angle;							/**< angle entre la direction du robot et la normale de l'obstacle */
};


/** \brief Liste des moteurs dynamixel
 */
enum mtDynamixelMotor_t {
	MOTOR_GRABBER_ARM = 1,
	MOTOR_GRABBER_FOREARM = 2,
	MOTOR_WHEEL_LEFT = 3,
	MOTOR_WHEEL_RIGHT = 4
};


/** \brief Calibrer un capteur de distance
 * 
 * Cette fonction permet de démarrer la procédure de calibration d'un capteur de
 * distance. La valeur brute du capteur de distance aparait sur l'afficheur 7 segments.
 * Le microcontrolleur doit être connecté à un ordinateur avec le moniteur série ouvert.
 * Lorsque que l'afficheur indique 0 l'utilisateur mesure la distance entre le capteur
 * et l'obstacle et l'écrit dans le moniteur série. Il répete l'opération pour la distance
 * maximum.
 * 
 * \param pin GPIO ou se situe le capteur de distance
 * \return capteur de distance à calibré
 */
mtObstacleSensor_t mtCalibrateObstacleSensor(int pin);


/** \brief Lire la valeur d'un capteur de distance
 * 
 * Cette fonction permet de lire la distance à laquelle se trouve un objet du capteur de
 * distance. La valeur du capteur est converti en mm grâce aux données issues de la calibration.
 * 
 * \param sensor capteur de distance à lire
 * \return distance en mm entre le capteur et l'obstacle
 */
float mtReadObstacleSensor(mtObstacleSensor_t &sensor);


/** \brief Détecter des objets devant le robot
 * 
 * Cette fonction permet de détecter la présence d'objets à l'avant du robot.
 * A partir des distances `d1` et `d2` données par les capteurs de distances 
 * Gauche et Droite, la fonction calcul une distance D moyenne entre le centre
 * du robot et l'obstacle. Elle calcul aussi l'angle A avec lequel le robot
 * approche l'obstacle.
 * 
 * ![](images/mtDetectObstacleFront.jpg)
 * 
 * \return distance et angle par rapport à l'origine du robot jusqu'à l'obstacle
 * 				détecté. `mtObstableResult.distance` est égal à -1 si aucun n'obstacle
 * 				n'est détecté. `mtObstableResult.angle` est égal à -90° ou 90° si seulement
 * 				un des capteurs détecte un obstacle
 */
mtObstableResult_t mtDetectObstacleFront();


/** \brief Détecter des objets derrière le robot
 * 
 * Cette fonction permet de détecter la présence d'objets à l'arrière du robot.
 * 
 * \return distance par rapport à l'origine du robot jusqu'à l'obstacle détecté.
 * 				`mtObstableResult.distance` est égal à -1 si aucun n'obstacle n'est détecté.
 * 				`mtObstableResult.angle` est toujours égal à 0
 */
mtObstableResult_t mtDetectObstacleRear();


/** \brief Tourner le carrousel
 * 
 * Cette fonction permet de tourner le moteur du carrousel.
 * 
 * \param speed vitesse de rotation comprise entre 1 et XXX pour tourner dans le sens
 * 			trigonométrique, entre -XXX et -1 pour tourner dans le sens anti-trigonométrique,
 * 			ou 0 pour arrêter la rotation.
 */
void mtRotateCarouselMotor(int speed);


/** \brief Tourner le carrousel relativement
 * 
 * Cette fonction permet de faire tourner le carrousel de `step` nombre d'emplacement
 * relativement à la position actuelle.
 * Une valeur positive tourne le carrousel dans le sens trigonométrique. Une valeur
 * négative, dans le sens anti-trigonométrique. 0 n'a aucun effet.
 * 
 * \param step nombre de d'emplacements à défiler
 */
void mtBumpCarouselPose(int step);


/** \brief Changer la position du carrousel 
 * 
 * Cette fonction permet de positionner le carrousel à un emplacement spécifique.
 * Le sens de rotation permettant le chemin le plus court est choisi.
 * 
 * \param location emplacement sur lequel se déplacer. Il doit être compris entre 0 et `nombre d'emplacement - 1`
 */
void mtSetCarouselPose(int location);


/** \brief Initialiser les 4 Dynamixel
 * 
 * Cette fonction permet d'initialiser les moteurs Dynamixel servant à mouvoir le bras robotisé
 * et à déplacer le robot. 
 * 
 * \sa mtSetPanelWheelPosition, mtStartPanelWheel, mtStopPanelWheel
 */
void mtInitDynamixelMotors();


/** \brief Tourner un moteur dynamixel
 * 
 * Cette fonction permet d'intéragir avec un moteur dynamixel selon deux mode :  
 * - en mode servo-moteur, en idiquant un angle à value
 * - en mode moteur continu, en indiquant une vitesse et un sens de rotation
 * 
 * \param motor indique le moteur à contrôler
 * \param mode indique le mode de contrôle (`MODE_SPEED` ou `MODE_POSE`)
 * \param value indique un angle en mode servo-moteur ou une vitesse en mode
 * 			moteur. une vitesse négative permet de faire tourner le moteur dans
 * 			le sens anti-trigonométrique
 */
void mtRotateDynamixelMotor(mtDynamixelMotor_t motor, mtDynamixelMode_t mode, int value);


/** \brief Deplacer le bras robotisé
 * 
 * Cette fonction déplace le bras robotisé linéairement à la position indiqué.
 * 
 * \param x, y destination du bras robotisé
 * \param speed vitesse de déplacement linéaire du bras
 */
void mtArmMove(int x, int y, int speed);


/** \brief Acancer le robot
 * 
 * Cette fonction permet de déplacer le robot tout droit. La distance parcouru
 * est calculée à partir du retour codeur de chaque moteur.
 * 
 * \param distance distance à parcourir en mm
 */
void mtMoveStraight(int distance);


/** \brief Pivoter le robot
 * 
 * Cette fonction permet de faire pivoter le robot sur lui-même. Un moteur tourne alors en marche
 * avant, et un moteur tourne en marche arrière. L'angle réalisé est estimé à l'aide des codeurs
 * de chaque moteur.
 * 
 * \param angle que le robot doit réaliser
 */
void mtMoveSpinAround(int angle);


/** \brief Ouvrir/Fermer pince
 * 
 * Cette fonction permet d'ouvrir et de fermer la pince du bras robotisé.
 * 
 * \param close true ferme la pince, false ouvre la pince
 */
void mtArmGripperSetClose(bool close);

#endif //MECATRONIQUE_MAIN_ESP_HPP
